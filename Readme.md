#Gothica

##Introduction
Gothica is an RPG centered miniatures game which aim to combine storytelling with miniature games. Adding in loved features of other
games in addition to some new thingies! Centered around giving players freedom, both in character creation, development and gameplay.
Moving around the board should feel dynamic!

### Abbrevations

**AT** *Activation Tracker*
**CT** *Clock Tracker*
**ED6** 			*Exploding 6 sided dice, when a 6 is rolled you take that dice and roll again with one additional dice.*
**IED6** 			*Same as ED6 but only the initial dices rolled can explode.*


### Accessories needed to play

**6 sided dice** 	*As many you can find.. really.*
** CT ** 			*One for each actor, can be simulated with 2x10 sided dice.*
**AT** 				*Just somwhere to place actor tokens in order of current CT to know who will get to activate. a Small sideboard will do.*

## Character creation

### Races
#### Humans
The human race is young and vast, split into many subfactions with a lot of conflict in between themselves. There are small differences in their physical forms but can be differentiated greatly by culture and heritage.

| Faction        	| STR 	| PHY 	| DEX  	| SPD	| INT 	| WIS  	| PRE  	| PER  	| RDEF 	| LCK	|
| ------------- 		|:-----:	| -----: 	| -----:	| -----:	| -----:	| -----:	| -----:	| -----:	| -----:	| -----:	|
| Brans      		|  3	  	| 3		|3		|3	   	|3		|3		|3		|3		|6		|3		|
| Karak      		|  2	  	| 3		|4		|3	   	|3		|2		|3		|4		|6		|3		|
| Durn      		|  4	  	| 4		|2		|3	   	|3		|3		|3		|3		|5		|3		|


### Derivative attributes


| Attribute | Formula |Description|
|-------------|-------------|---------------|
|MEC | STR+DEX/2 | Meele Combat attack value
|RAC| PER+DEX/2| Range Combat attack value
|MAC|INT+WIS/2| Magic Combat attack value
|MOV| SPD+DEX/2| Base movement of a character
|DEF|RDEF+(DEX+PHY/2) | Defense value against attacks
|HTP|PHY+PRE*3 | Hitpoints of damage you can take before going down
|FAT|WIS+PRE | Fatigue, primary resource for using magic
|FATG|WIS+PRE/2 | Fatigue Regen, amount of FAT that is recovered each activation
|STA|PHY+STR | Stamina, primary resource for physical skills
|STAG|PHY+STR/2 |Stamina Regen, amount of STA that is recovered each activation
|CMD|PRE+INT/2 | Command, morale of a character and it's influence on others

### Classes
#### Multiclassing
When multiclassing you have two active classes assigned to your character. 
The character decides for a primary class and a secondary class. Any growth earned is given to the primary class and none to the secondary. The secondary class is mainly used to complement the primary in some manner. Having a secondary class enables the character to use that class-specific skills and abilities.

Secondary classes does not gain benefit from class attribute bonuses and cannot use skills that require it to be the primary class on the character. 

##### Squire
| Attribute        	| STR 	| MEC 	| 
| ------------- 		|:-----:	| -----: 	| 
| Value      		|  +1	| +1	|

##### Thief
| Attribute        	| DEF 	| DEX 	| 
| ------------- 		|:-----:	| -----: 	| 
| Value      		|  +1	| +1	|

##### Scholar
| Attribute        	| INT 	| WIS 	| 
| ------------- 		|:-----:	| -----: 	| 
| Value      		|  +1	| +1	|

##### Disciple
| Attribute        	| INT 	| MAC 	| 
| ------------- 		|:-----:	| -----: 	| 
| Value      		|  +1	| +1	|

##### Ranger
| Attribute        	| SPD 	| PER 	| 
| ------------- 		|:-----:	| -----: 	| 
| Value      		|  +1	| +1	|

### Character development
a Character develops in many ways. Gaining experience and level on it's own in addition to doing the same for classes to unlock specific powers and abilities.

#### Character growth

##### Experience
a Character gains experience from many things, both in combat and outside of it doing crafts or other things. When a character reaches a certain amount of experience they level up.

##### Levels
a Character starts out at level 1 and work towards the max level of 30. For each  level the amount of needed experience to go to the next level is increased.


#### Class growth

##Combat
Combat is played out on a board of some sort. Players are made up of either a dungeon master vs x amount of players (PvG) or players vs other players (PvP). The combat system is time oriented, time is a resource which you need to manage or at least respect how the enemy manages theirs!


### The game
One game consist of multiple turns determined by the game master or players together.
### The turn
One turn goes on until all actors have activated at least once each
### The activation
an Actor may activate when it reaches 100 on it's CT tracker and is first on the AT.
Actors may activate multiple times during a turn.

#### Acting
There are a few basic things an actor can do during it's activation. Each cost a certain amount of CT.
Each action have a cost specified in the first pair of square brackets ([...]) and if there are any limits on how many times
it may be used during a turn it's specified by a second pair of square brackets.

- Move 	 [50][1] *Move your MOV in inches*
- Run 	 [100][1] *Move your MOVx1.5 in inches*
- Attack 	 [50] *Make an attack with each equiped weapon,  attacks and their cost can be modified by skills*
- Use skill [x+] *Each skill will have a specific cost*
- Pass 	 [25+][1] *Passing mean's that the actor does not want to do anything and can remove 25 or more CT from it's tracker*

#### Attack and damage rolls
Both attack and damage rolls have a base of 2IED6.

**Attack rolls**: Add the result of dice to their attack value of given attack type (MEC,RAC,MAC) and need to be equal to or higher than the targets DEF to hit.

**Damage rolls**: Add the result of dice to the attackers combined attack power and then subtract the targets ARM value. This is the amount of damage that has been done. *Note, depending of the type of attack the attack power is calculated differently*

#### Out of turn acting
When an actor is attacked it can use defensive skills to react to the agressor. This behaves just the same as during a regular activation only that the primary skill used must be from the defensive category.

*If an actor get's attacked and have the defensive skill counter attack it can be activated after the incoming attack have been resolved. Counter attack can then be combined with offensive skills like backswing or powerful attack.*

### After the activation
The just activated actor is moved down on the AT depending on how much CT was spent during the activation. After this all other actors gains
an amount of CT equal to their SPD.